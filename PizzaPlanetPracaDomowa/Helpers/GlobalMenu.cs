﻿using PizzaPlanet.Business;
using PizzaPlanet.Business.Interfaces;
using PizzaPlanet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Cli.Helpers
{
    internal class GlobalMenu
    {
        IIgredientService _igredientService;
        IOrderService _orderService;
        IPizzaService _pizzaService;
        ISauceService _sauceService;
        IClientService _clientService;

        public GlobalMenu(IIgredientService igredientService, IOrderService orderService, IPizzaService pizzaService, ISauceService sauceService, IClientService clientService)
        {
            _igredientService = igredientService;
            _orderService = orderService;
            _pizzaService = pizzaService;
            _sauceService = sauceService;
            _clientService = clientService;
        }

        private IoHelper _ioHelper = new IoHelper();

        public void CheckCondition(string option)
        {
            switch (option)
            {
                case "i":
                    Console.WriteLine("Wybrales opcje: Add Ingredients");
                    AddIngredient();
                    break;
                case "s":
                    Console.WriteLine("Wybrales opcje: Add Sauce");
                    AddSauce();
                    break;
                case "p":
                    Console.WriteLine("Wybrales opcje: Add Pizza");
                    _ioHelper.PrintGeneralInfoAboutCreatingPizza();
                    PrintAllIngredients();
                    PrintAllSauces();
                    AddPizza();
                    break;
                case "m":
                    Console.WriteLine("Wybrales opcje: Show Menu");
                    PrintAllPizzas();
                    break;
                case "o":
                    Console.WriteLine("Wybrales opcje: Order");
                    _ioHelper.PrintOrderInformation();
                    PrintAllPizzas();
                    PrintAllSauces();
                    AddClient();
                    break;
                case "e":
                    Console.WriteLine("Wybrales opcje: Exit");
                    break;
            }
        }

        private void PrintAllPizzas()
        {
            var foundPizzas = _pizzaService.GetAllPizzas();
            _ioHelper.PrintPizzas(foundPizzas);
        }

        private void PrintAllIngredients()
        {
            var foundIngredients = _igredientService.GetAllIgredients();
            _ioHelper.PrintIngredients(foundIngredients);
        }

        private void PrintAllSauces()
        {
            var foundSauces = _sauceService.GetAllSauces();
            _ioHelper.PrintSauces(foundSauces);
        }

        public void AddIngredient()
        {
            string ingredientName = ConsoleUtilities.GetStringFromConsole("Jaka nazwe skladnika chcesz dodac?");

            var igredient = new IgredientBl
            {
                Name = ingredientName
            };
            try
            {
                _igredientService.AddIgredient(igredient);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void AddSauce()
        {
            string souceName = ConsoleUtilities.GetStringFromConsole("Jaka nazwe sosu chcesz dodac?");
            double price = ConsoleUtilities.GetPriceFromConsole("Jaka cene sosu chcesz dodac?");

            var sauce = new SauceBl
            {
                Name = souceName,
                Price = price
            };
            try
            {
                _sauceService.AddSauce(sauce);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void AddPizza()
        {
            string pizzaName = ConsoleUtilities.GetStringFromConsole("Podaj nazwe pizzy");
            double pizzaPriceOfSmallSize = ConsoleUtilities.GetPriceFromConsole("Jaka cene pizzy dla rozmiaru S chcesz dodac?");
            double pizzaPriceOfMediumSize = ConsoleUtilities.GetPriceFromConsole("Jaka cene pizzy dla rozmiaru M chcesz dodac?");
            double pizzaPriceOfLargeSize = ConsoleUtilities.GetPriceFromConsole("Jaka cene pizzy dla rozmiaru L chcesz dodac?");
            int chooseSauce = ConsoleUtilities.GetIntFromConsole("Podaj id sosu, ktory chcesz wybrac?");
            string doYouWantAddNewIngredient = ConsoleUtilities.GetStringFromConsole("Czy chcesz dodac skladnik? Wpisz Yes lub No");
            List<int> IngredientsChosenList = new List<int>();
            while (doYouWantAddNewIngredient == "Yes")
            {
                int ingredients = ConsoleUtilities.GetIntFromConsole("Podaj id skladnika, ktory chcesz dodac?");
                IngredientsChosenList.Add(ingredients);
                doYouWantAddNewIngredient = ConsoleUtilities.GetStringFromConsole("Czy chcesz dodac nastepny skladnik? Wpisz Yes lub No");

            }
            var pizza = new PizzaBl
            {
                Name = pizzaName,
                PriceOfSmallSize = pizzaPriceOfSmallSize,
                PriceOfMediumSize = pizzaPriceOfMediumSize,
                PriceOfLargeSize = pizzaPriceOfLargeSize,
                SauceId = chooseSauce,
                IngredientsId = IngredientsChosenList
            };
            try
            {
                _pizzaService.AddPizza(pizza);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void AddOrder(ClientBl client)
        {
            DateTime data = DateTime.UtcNow;
            var dayOfWeek = data.DayOfWeek.ToString();

            List<string> pizzaSizesList = new List<string>();

            string doYouWantAddNewPizza = ConsoleUtilities.GetStringFromConsole("Czy chcesz dodac pizze? Wpisz Yes lub No");
            List<int> PizzaIdList = new List<int>();

            while (doYouWantAddNewPizza == "Yes")
            {
                int idPizza = ConsoleUtilities.GetIntFromConsole("Wpisz id pizzy, ktora chcesz dodac");
                PizzaIdList.Add(idPizza);
                doYouWantAddNewPizza = ConsoleUtilities.GetStringFromConsole("Czy chcesz dodac nastepna pizze? Wpisz Yes lub No");
            }

            for (int i = 0; i < PizzaIdList.Count; i++)
            {
                string pizzaSize = ConsoleUtilities.GetStringFromConsole($"Jaki rozmiar dla pizzy {PizzaIdList[i]} chcesz wybrac?");
                pizzaSizesList.Add(pizzaSize);
            }

            var orderPizza = new OrderBl()
            {
                PizzaId = PizzaIdList,
                PizzaSizes = pizzaSizesList,
                Client = client
            };

            try
            {
                _orderService.AddPizzaToOrder(orderPizza);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            ConsoleUtilities.GetStringFromConsole($"Mozesz dodac maksymalnie {_orderService.GratisSaucesCount(orderPizza)} darmowych sosow");

            string doYouWantAddGratisSauces = ConsoleUtilities.GetStringFromConsole("Czy chcesz dodac gratisowe sosy? Wpisz Yes lub No");
            List<int> GratisSaucesChosenList = new List<int>();

            List<int> SaucesId = new List<int>();

            while (doYouWantAddGratisSauces == "Yes")
            {
                int gratisSaucesId = ConsoleUtilities.GetIntFromConsole("Podaj id sosu, ktory chcesz dodac");
                GratisSaucesChosenList.Add(gratisSaucesId);
                SaucesId.Add(gratisSaucesId);
                doYouWantAddGratisSauces = ConsoleUtilities.GetStringFromConsole("Czy chcesz dodac nastepny gratisowy sos? Wpisz Yes lub No");
            }

            string doYouWantChoosePaidSauces = ConsoleUtilities.GetStringFromConsole("Czy chcesz wybrac dodatkowo platne sosy? Wpisz Yes lub No");
            List<int> PaidSaucesChosenList = new List<int>();

            while (doYouWantChoosePaidSauces == "Yes")
            {
                int paidSaucesId = ConsoleUtilities.GetIntFromConsole("Podaj Id sosu, ktory chcesz wybrac");
                PaidSaucesChosenList.Add(paidSaucesId);
                SaucesId.Add(paidSaucesId);
                doYouWantChoosePaidSauces = ConsoleUtilities.GetStringFromConsole("Czy chcesz dodac nastepny platny sos? Wpisz Yes lub No");
            }

            var orderSauces = new OrderBl
            {
                PizzaId = PizzaIdList,
                PizzaSizes = pizzaSizesList,
                GratisSaucesId = GratisSaucesChosenList,
                PaidSaucesId = PaidSaucesChosenList,
                SaucesId = SaucesId,
                Client = client
            };

            try
            {
                _orderService.AddGratisSaucesToOrder(orderSauces);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }

            var totalPrice = _orderService.CalculateOrder(orderSauces,dayOfWeek);

            var order = new OrderBl
            {
                PizzaId = PizzaIdList,
                PizzaSizes = pizzaSizesList,
                GratisSaucesId = GratisSaucesChosenList,
                PaidSaucesId = PaidSaucesChosenList,
                SaucesId = SaucesId,
                TotalPrice = totalPrice,
                Client = client
            };

            Console.WriteLine($"Calkowita cena zamowienia wynosi: {totalPrice}");

            try
            {
                _orderService.AddOrder(order);
            }
            catch (Exception e)
            {
                Console.WriteLine(e.Message);
            }
        }

        public void AddClient()
        {
            int clientNumber = ConsoleUtilities.GetIntFromConsole("Podaj numer telefonu klienta");

            var clientNr = new ClientBl()
            {
                TelephoneNumber = clientNumber
            };

            bool doesClientExist = _clientService.CheckingIfClientExist(clientNr);

            if (doesClientExist)
            {
                try
                {

                    DateTime dataOfLastOrder = DateTime.UtcNow;
                    var clientData = new ClientBl()
                    {
                        TelephoneNumber = clientNumber,
                        DataOfLastOrder = dataOfLastOrder
                    };

                    _clientService.UpdateDataInClient(clientNumber, dataOfLastOrder);

                    AddOrder(clientData);
                }
                catch (Exception e)
                {

                    Console.WriteLine(e.Message);
                }
            }

            if (!doesClientExist)
            {
                try
                {
                    string email = ConsoleUtilities.GetStringFromConsole("Podaj E-mail");
                    string name = ConsoleUtilities.GetStringFromConsole("Podaj Imie");
                    string surname = ConsoleUtilities.GetStringFromConsole("Podaj nazwisko");
                    string address = ConsoleUtilities.GetStringFromConsole("Podaj adres");
                    DateTime dataOfLastOrder = DateTime.UtcNow;

                    var client = new ClientBl
                    {
                        TelephoneNumber = clientNumber,
                        EMail = email,
                        Name = name,
                        Surname = surname,
                        Address = address,
                        DataOfLastOrder = dataOfLastOrder
                    };

                    var addedClient = _clientService.AddClient(client);

                    AddOrder(addedClient);
                }
                catch (Exception e)
                {
                    Console.WriteLine(e.Message);
                }

            }
        }
    }

}
