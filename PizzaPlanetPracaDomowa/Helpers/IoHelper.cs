﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaPlanet.Business.Models;
namespace PizzaPlanet.Cli.Helpers
{
    internal class IoHelper
    {
        public void PrintIngredients(IEnumerable<IgredientBl> igredients)
        {
            foreach (var igredient in igredients)
            {
                Console.WriteLine($"Dostepne skladniki to {igredient.Name}" + $" Id skladnika to: {igredient.Id}");
            }
        }

        public void PrintSauces(IEnumerable<SauceBl> sauces)
        {
            foreach (var sauce in sauces)
            {
                Console.WriteLine($"Dostepne sosy to {sauce.Name}" + $" Id sosu to: {sauce.Id}");
            }
        }

        public void PrintPizzas(IEnumerable<PizzaBl> pizzas)
        {
            foreach (var pizza in pizzas)
            {
                Console.WriteLine($"Pizza:\n Nazwa: {pizza.Name}"+ $" \n PizzaId: {pizza.Id} " + 
                    $" \n Cena w rozmiarze S: {pizza.PriceOfSmallSize} " +
                   $"\n Cena w rozmiarze M: {pizza.PriceOfMediumSize}" + $" \n Cena w rozmiarze L: {pizza.PriceOfLargeSize} " +
                   $"\n Sos na ciasto: {String.Join(",", pizza.SauceId)} " + $" \n Skladniki: {String.Join(",", pizza.IngredientsId)}");
            }
        }

        public void PrintGeneralInfoAboutCreatingPizza()
        {
            Console.WriteLine("Masz mozliwosc dodania maksymalnie 7 skladnikow.");
            Console.WriteLine("Jeden skladnik nie moze powtarzac sie wiecej niz 2 razy.");
            Console.WriteLine("Masz mozliwosc dodania tylko 1 sosu.");
        }

        public void PrintOrderInformation()
        {
            Console.WriteLine("Mozesz wybrac dowolna ilosc pizz. Kazda pizze mozesz wybrac w jednym z wymienionych rozmiarow: S, M, L");
            Console.WriteLine("Kazda pizza w rozmiarze L otrzymuje za darmo dwa dodatkowe sosy");
            Console.WriteLine("Kazda pizza w rozmiarze S i M otrzymuje za darmo jeden dodatkowy sos");
            Console.WriteLine("Mozna dobrac dowolna ilosc dodatkowych sosow");
            Console.WriteLine("Dodatkowe sosy sa platne");
        }
    }
}
