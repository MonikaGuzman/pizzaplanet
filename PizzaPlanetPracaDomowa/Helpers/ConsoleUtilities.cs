﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Business;
using PizzaPlanet.Cli.Helpers;
namespace PizzaPlanet.Cli
{
    static class ConsoleUtilities
    {
        public static string GetStringFromConsole(string message)
        {
                Console.WriteLine(message);
                string returnMessage = Console.ReadLine();
                return returnMessage;
        }

        public static int GetIntFromConsole(string message)
        {
            int number;

            while (!int.TryParse(GetStringFromConsole(message), out number))
            {
                Console.WriteLine("Nie wpisales liczby");
            }

            return number;
        }

        public static double GetPriceFromConsole(string message)
        {
            bool result = false;
            double price = 0;
            while (result == false)
            {
                Console.WriteLine(message);
                string value = Console.ReadLine();
                result = double.TryParse(value, out price);
                if (result == false)
                {
                    Console.WriteLine($"double.TryParse nie mogl zmienic {value} na double");
                }
            }
            return Math.Round(price, 2);
        }
    }
}
