﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Business;
using PizzaPlanet.Cli.Helpers;
using Ninject;

namespace PizzaPlanet.Cli
{
    public class Program
    {
        IKernel kernel = new StandardKernel(new ModulePizzaPlanet());

        static void Main(string[] args)
        {
            string closeProgram = "";
            Console.WriteLine("Witaj w PizzaPlanet");

            while (closeProgram != "e")
            {
                DisplayMenu();
                string option = ConsoleUtilities.GetStringFromConsole("Jaka opcje wybierasz?");
                if (option == "i" || option == "s" || option == "p" || option == "m" || option == "o" || option == "e")
                {
                    closeProgram = option;

                    var program = new Program();
                    try
                    {
                        program.Start(option);
                    }
                    catch (Exception e)
                    {

                        Console.WriteLine(e.Message);
                    }
                }
                else
                {
                    Console.WriteLine("Nie wybrales zadnej z opcji");
                }
            }
        }

        public void Start(string option)
        {
            var _globalMenu = kernel.Get<GlobalMenu>();
            _globalMenu.CheckCondition(option);
        }

        public static void DisplayMenu()
        {
            Console.WriteLine("Opcje do wyboru: ");
            Console.WriteLine("i - Add Ingredients");
            Console.WriteLine("s - Add Sauce");
            Console.WriteLine("p - Add Pizza");
            Console.WriteLine("m - Show Menu");
            Console.WriteLine("o - Order");
            Console.WriteLine("e - Exit");
        }
    }
}
