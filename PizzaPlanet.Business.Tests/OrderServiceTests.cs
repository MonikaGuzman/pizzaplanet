﻿using Moq;
using NUnit.Framework;
using PizzaPlanet.Business.Interfaces;
using PizzaPlanet.Business.Models;
using System;
using System.Collections.Generic;
using System.Linq;

namespace PizzaPlanet.Business.Tests
{
    [TestFixture]
    public class OrderServiceTests
    {
        [Test]
        public void TotalPriceOfOrderedPaidSauces_ValidInput_ValidOtput()
        {
            //Arrange
            var sauceNr1 = new SauceBl()
            {
                Id = 1,
                Name = "pomidorowy",
                Price = 4,
            };

            var sauceNr2 = new SauceBl()
            {
                Id = 2,
                Name = "czosnkowy",
                Price = 2,
            };

            var order = new OrderBl()
            {
                PaidSaucesId = new List<int>() { sauceNr1.Id, sauceNr2.Id },
            };

            var testListOfSauces = new List<SauceBl>();
            testListOfSauces.Add(sauceNr1);
            testListOfSauces.Add(sauceNr2);

            var orderServiceMock = new Mock<ISauceService>();
            orderServiceMock.Setup(x => x.GetAllSauces()).Returns(testListOfSauces);

            var orderService = new OrderService(null,orderServiceMock.Object, null);

            //Act

            var result = orderService.TotalPriceOfOrederedPaidSauces(order);

            //Assert

            Assert.AreEqual(6, result);
        }

        [Test]
        public void TotalPriceOfOrderWithoutPaidSauces_MondayDayAndAllSizeOfPizzas_ValidPrice()
        {
            //Arrange

            var dayOfWeek = "Monday";

            var pizzaNr1 = new PizzaBl()
            {
                Id = 1,
                Name = "margaritta",
                PriceOfSmallSize = 10,
                PriceOfMediumSize = 20,
                PriceOfLargeSize = 30,
                IngredientsId = new List<int>() { 2, 4 },
                SauceId = 1,
            };

            var pizzaNr2 = new PizzaBl()
            {
                Id = 2,
                Name = "wlasna",
                PriceOfSmallSize = 40,
                PriceOfMediumSize = 60,
                PriceOfLargeSize = 70,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var pizzaNr3 = new PizzaBl()
            {
                Id = 3,
                Name = "dobra",
                PriceOfSmallSize = 20,
                PriceOfMediumSize = 40,
                PriceOfLargeSize = 50,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var testListOfPizzas = new List<PizzaBl>();

            testListOfPizzas.Add(pizzaNr1);
            testListOfPizzas.Add(pizzaNr2);
            testListOfPizzas.Add(pizzaNr3);


            var order = new OrderBl()
            {
                PizzaId = new List<int>() { pizzaNr1.Id, pizzaNr2.Id, pizzaNr3.Id },
                PizzaSizes = new List<string>() { "S", "M", "L" }
            };

            var orderServiceMock = new Mock<IPizzaService>();
            orderServiceMock.Setup(x => x.GetAllPizzas()).Returns(testListOfPizzas);

            var orderService = new OrderService(null, null, orderServiceMock.Object);

            //Act

            var result = orderService.TotalPriceOfOrderWithoutPaidSauces(order, dayOfWeek);

            //Assert

            Assert.AreEqual(120, result);
        }

        [Test]
        public void TotalPriceOfOrderWithoutPaidSauces_WednesdayDayAndAllSizeOfPizzas_ValidPrice()
        {
            //Arrange

            var dayOfWeek = "Wednesday";

            var pizzaNr1 = new PizzaBl()
            {
                Id = 1,
                Name = "margaritta",
                PriceOfSmallSize = 10,
                PriceOfMediumSize = 20,
                PriceOfLargeSize = 30,
                IngredientsId = new List<int>() { 2, 4 },
                SauceId = 1,
            };

            var pizzaNr2 = new PizzaBl()
            {
                Id = 2,
                Name = "wlasna",
                PriceOfSmallSize = 40,
                PriceOfMediumSize = 60,
                PriceOfLargeSize = 70,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var pizzaNr3 = new PizzaBl()
            {
                Id = 3,
                Name = "dobra",
                PriceOfSmallSize = 20,
                PriceOfMediumSize = 40,
                PriceOfLargeSize = 50,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var testListOfPizzas = new List<PizzaBl>();

            testListOfPizzas.Add(pizzaNr1);
            testListOfPizzas.Add(pizzaNr2);
            testListOfPizzas.Add(pizzaNr3);

            var order = new OrderBl()
            {
                PizzaId = new List<int>() { pizzaNr1.Id, pizzaNr2.Id, pizzaNr3.Id },
                PizzaSizes = new List<string>() { "S", "M", "L" }
            };

            var orderServiceMock = new Mock<IPizzaService>();
            orderServiceMock.Setup(x => x.GetAllPizzas()).Returns(testListOfPizzas);

            var orderService = new OrderService(null, null, orderServiceMock.Object);

            //Act

            var result = orderService.TotalPriceOfOrderWithoutPaidSauces(order, dayOfWeek);

            //Assert

            Assert.AreEqual(110, result);
        }

        [Test]
        public void TotalPriceOfOrderWithoutPaidSauces_SundayDayAndAllSizeOfPizzas_ValidPrice()
        {
            //Arrange

            var dayOfWeek = "Sunday";

            var pizzaNr1 = new PizzaBl()
            {
                Id = 1,
                Name = "margaritta",
                PriceOfSmallSize = 10,
                PriceOfMediumSize = 20,
                PriceOfLargeSize = 30,
                IngredientsId = new List<int>() { 2, 4 },
                SauceId = 1,
            };

            var pizzaNr2 = new PizzaBl()
            {
                Id = 2,
                Name = "wlasna",
                PriceOfSmallSize = 40,
                PriceOfMediumSize = 60,
                PriceOfLargeSize = 70,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var pizzaNr3 = new PizzaBl()
            {
                Id = 3,
                Name = "dobra",
                PriceOfSmallSize = 20,
                PriceOfMediumSize = 40,
                PriceOfLargeSize = 50,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var testListOfPizzas = new List<PizzaBl>();

            testListOfPizzas.Add(pizzaNr1);
            testListOfPizzas.Add(pizzaNr2);
            testListOfPizzas.Add(pizzaNr3);

            var order = new OrderBl()
            {
                PizzaId = new List<int>() { pizzaNr1.Id, pizzaNr2.Id, pizzaNr3.Id },
                PizzaSizes = new List<string>() { "S", "M", "L" }
            };

            var orderServiceMock = new Mock<IPizzaService>();
            orderServiceMock.Setup(x => x.GetAllPizzas()).Returns(testListOfPizzas);

            var orderService = new OrderService(null, null, orderServiceMock.Object);

            //Act

            var result = orderService.TotalPriceOfOrderWithoutPaidSauces(order, dayOfWeek);

            //Assert

            Assert.AreEqual(60, result);
        }

        [Test]
        public void TotalPriceOfOrderWithoutPaidSauces_SaturdayDayAndAllSizeOfPizzas_ValidPrice()
        {
            //Arrange

            var dayOfWeek = "Saturday";

            var pizzaNr1 = new PizzaBl()
            {
                Id = 1,
                Name = "margaritta",
                PriceOfSmallSize = 10,
                PriceOfMediumSize = 20,
                PriceOfLargeSize = 30,
                IngredientsId = new List<int>() { 2, 4 },
                SauceId = 1,
            };

            var pizzaNr2 = new PizzaBl()
            {
                Id = 2,
                Name = "wlasna",
                PriceOfSmallSize = 40,
                PriceOfMediumSize = 60,
                PriceOfLargeSize = 70,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var pizzaNr3 = new PizzaBl()
            {
                Id = 3,
                Name = "dobra",
                PriceOfSmallSize = 20,
                PriceOfMediumSize = 40,
                PriceOfLargeSize = 50,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var testListOfPizzas = new List<PizzaBl>();

            testListOfPizzas.Add(pizzaNr1);
            testListOfPizzas.Add(pizzaNr2);
            testListOfPizzas.Add(pizzaNr3);

            var order = new OrderBl()
            {
                PizzaId = new List<int>() { pizzaNr1.Id, pizzaNr2.Id, pizzaNr3.Id },
                PizzaSizes = new List<string>() { "S", "M", "L" }
            };

            var orderServiceMock = new Mock<IPizzaService>();
            orderServiceMock.Setup(x => x.GetAllPizzas()).Returns(testListOfPizzas);

            var orderService = new OrderService(null, null, orderServiceMock.Object);

            //Act

            var result = orderService.TotalPriceOfOrderWithoutPaidSauces(order, dayOfWeek);

            //Assert

            Assert.AreEqual(60, result);
        }

        [Test]
        public void GratisSaucesCount_OnlySmallSizesPizzas_ValidOutput()
        {
            //Arrange

            var pizzaNr1 = new PizzaBl()
            {
                Id = 1,
                Name = "margaritta",
                PriceOfSmallSize = 10,
                PriceOfMediumSize = 20,
                PriceOfLargeSize = 30,
                IngredientsId = new List<int>() { 2, 4 },
                SauceId = 1,
            };

            var pizzaNr2 = new PizzaBl()
            {
                Id = 2,
                Name = "wlasna",
                PriceOfSmallSize = 40,
                PriceOfMediumSize = 60,
                PriceOfLargeSize = 70,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var pizzaNr3 = new PizzaBl()
            {
                Id = 3,
                Name = "dobra",
                PriceOfSmallSize = 20,
                PriceOfMediumSize = 40,
                PriceOfLargeSize = 50,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var testListOfPizzas = new List<PizzaBl>();

            testListOfPizzas.Add(pizzaNr1);
            testListOfPizzas.Add(pizzaNr2);
            testListOfPizzas.Add(pizzaNr3);

            var order = new OrderBl()
            {
                PizzaSizes = new List<string>() { "S", "S" , "S"}
            };

            var orderService = new OrderService(null, null, null);

            //Act

            var result = orderService.GratisSaucesCount(order);

            //Assert

            Assert.AreEqual(3, result);
        }

        [Test]
        public void GratisSaucesCount_OnlyMediumSizesPizzas_ValidOutput()
        {
            //Arrange

            var pizzaNr1 = new PizzaBl()
            {
                Id = 1,
                Name = "margaritta",
                PriceOfSmallSize = 10,
                PriceOfMediumSize = 20,
                PriceOfLargeSize = 30,
                IngredientsId = new List<int>() { 2, 4 },
                SauceId = 1,
            };

            var pizzaNr2 = new PizzaBl()
            {
                Id = 2,
                Name = "wlasna",
                PriceOfSmallSize = 40,
                PriceOfMediumSize = 60,
                PriceOfLargeSize = 70,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var pizzaNr3 = new PizzaBl()
            {
                Id = 3,
                Name = "dobra",
                PriceOfSmallSize = 20,
                PriceOfMediumSize = 40,
                PriceOfLargeSize = 50,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var testListOfPizzas = new List<PizzaBl>();

            testListOfPizzas.Add(pizzaNr1);
            testListOfPizzas.Add(pizzaNr2);
            testListOfPizzas.Add(pizzaNr3);

            var order = new OrderBl()
            {
                PizzaSizes = new List<string>() { "M", "M", "M" }
            };

            var orderService = new OrderService(null, null, null);

            //Act

            var result = orderService.GratisSaucesCount(order);

            //Assert

            Assert.AreEqual(3, result);
        }

        [Test]
        public void GratisSaucesCount_OnlyLargeSizesPizzas_ValidOutput()
        {
            //Arrange

            var pizzaNr1 = new PizzaBl()
            {
                Id = 1,
                Name = "margaritta",
                PriceOfSmallSize = 10,
                PriceOfMediumSize = 20,
                PriceOfLargeSize = 30,
                IngredientsId = new List<int>() { 2, 4 },
                SauceId = 1,
            };

            var pizzaNr2 = new PizzaBl()
            {
                Id = 2,
                Name = "wlasna",
                PriceOfSmallSize = 40,
                PriceOfMediumSize = 60,
                PriceOfLargeSize = 70,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var pizzaNr3 = new PizzaBl()
            {
                Id = 3,
                Name = "dobra",
                PriceOfSmallSize = 20,
                PriceOfMediumSize = 40,
                PriceOfLargeSize = 50,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var testListOfPizzas = new List<PizzaBl>();

            testListOfPizzas.Add(pizzaNr1);
            testListOfPizzas.Add(pizzaNr2);
            testListOfPizzas.Add(pizzaNr3);

            var order = new OrderBl()
            {
                PizzaSizes = new List<string>() { "L", "L", "L" }
            };

            var orderService = new OrderService(null, null, null);

            //Act

            var result = orderService.GratisSaucesCount(order);

            //Assert

            Assert.AreEqual(6, result);
        }

        [Test]
        public void GratisSaucesCount_OnlyAllSizesPizzas_ValidOutput()
        {
            //Arrange

            var pizzaNr1 = new PizzaBl()
            {
                Id = 1,
                Name = "margaritta",
                PriceOfSmallSize = 10,
                PriceOfMediumSize = 20,
                PriceOfLargeSize = 30,
                IngredientsId = new List<int>() { 2, 4 },
                SauceId = 1,
            };

            var pizzaNr2 = new PizzaBl()
            {
                Id = 2,
                Name = "wlasna",
                PriceOfSmallSize = 40,
                PriceOfMediumSize = 60,
                PriceOfLargeSize = 70,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var pizzaNr3 = new PizzaBl()
            {
                Id = 3,
                Name = "dobra",
                PriceOfSmallSize = 20,
                PriceOfMediumSize = 40,
                PriceOfLargeSize = 50,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var testListOfPizzas = new List<PizzaBl>();

            testListOfPizzas.Add(pizzaNr1);
            testListOfPizzas.Add(pizzaNr2);
            testListOfPizzas.Add(pizzaNr3);

            var order = new OrderBl()
            {
                PizzaSizes = new List<string>() { "S", "M", "L" }
            };

            var orderService = new OrderService(null, null, null);

            //Act

            var result = orderService.GratisSaucesCount(order);

            //Assert

            Assert.AreEqual(4, result);
        }

        [Test]
        public void CalculateOrder_IfPaidSaucesAreChosen_ValidOutput()
        {
            //Arrange

            var dayOfWeek = "Monday";

            var sauceNr1 = new SauceBl()
            {
                Id = 1,
                Name = "pomidorowy",
                Price = 4,
            };

            var sauceNr2 = new SauceBl()
            {
                Id = 2,
                Name = "czosnkowy",
                Price = 2,
            };

            var pizzaNr1 = new PizzaBl()
            {
                Id = 1,
                Name = "margaritta",
                PriceOfSmallSize = 10,
                PriceOfMediumSize = 20,
                PriceOfLargeSize = 30,
                IngredientsId = new List<int>() { 2, 4 },
                SauceId = 1,
            };

            var pizzaNr2 = new PizzaBl()
            {
                Id = 2,
                Name = "wlasna",
                PriceOfSmallSize = 40,
                PriceOfMediumSize = 60,
                PriceOfLargeSize = 70,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var testListOfPizzas = new List<PizzaBl>();

            testListOfPizzas.Add(pizzaNr1);
            testListOfPizzas.Add(pizzaNr2);

            var testListOfSauces = new List<SauceBl>();
            testListOfSauces.Add(sauceNr1);
            testListOfSauces.Add(sauceNr2);

            var order = new OrderBl()
            {
                PaidSaucesId = new List<int>() { sauceNr1.Id, sauceNr2.Id },
                PizzaId = new List<int>() { pizzaNr1.Id, pizzaNr2.Id },
                PizzaSizes = new List<string>() { "M", "L" }
            };

            var orderServicePizzaMock = new Mock<IPizzaService>();
            var orderServiceSauceMock = new Mock<ISauceService>();

            orderServicePizzaMock.Setup(x => x.GetAllPizzas()).Returns(testListOfPizzas);
            orderServiceSauceMock.Setup(x => x.GetAllSauces()).Returns(testListOfSauces);

            var orderService = new OrderService(null, orderServiceSauceMock.Object, orderServicePizzaMock.Object);

            //Act

            var result = orderService.CalculateOrder(order, dayOfWeek);

            //Assert

            Assert.AreEqual(96, result);
        }

        [Test]
        public void CalculateOrder_IfPaidSaucesAreNull_ValidOutput()
        {
            //Arrange

            var dayOfWeek = "Monday";

            var sauceNr1 = new SauceBl()
            {
                Id = 1,
                Name = "pomidorowy",
                Price = 4,
            };

            var sauceNr2 = new SauceBl()
            {
                Id = 2,
                Name = "czosnkowy",
                Price = 2,
            };

            var pizzaNr1 = new PizzaBl()
            {
                Id = 1,
                Name = "margaritta",
                PriceOfSmallSize = 10,
                PriceOfMediumSize = 20,
                PriceOfLargeSize = 30,
                IngredientsId = new List<int>() { 2, 4 },
                SauceId = 1,
            };

            var pizzaNr2 = new PizzaBl()
            {
                Id = 2,
                Name = "wlasna",
                PriceOfSmallSize = 40,
                PriceOfMediumSize = 60,
                PriceOfLargeSize = 70,
                IngredientsId = new List<int>() { 1, 3 },
                SauceId = 2,
            };

            var testListOfPizzas = new List<PizzaBl>();

            testListOfPizzas.Add(pizzaNr1);
            testListOfPizzas.Add(pizzaNr2);

            var testListOfSauces = new List<SauceBl>();
            testListOfSauces.Add(sauceNr1);
            testListOfSauces.Add(sauceNr2);

            var order = new OrderBl()
            {
                PaidSaucesId = new List<int>(),
                PizzaId = new List<int>() { pizzaNr1.Id, pizzaNr2.Id },
                PizzaSizes = new List<string>() { "M", "L" }
            };

            var orderServicePizzaMock = new Mock<IPizzaService>();
            var orderServiceSauceMock = new Mock<ISauceService>();

            orderServicePizzaMock.Setup(x => x.GetAllPizzas()).Returns(testListOfPizzas);
            orderServiceSauceMock.Setup(x => x.GetAllSauces()).Returns(testListOfSauces);

            var orderService = new OrderService(null, orderServiceSauceMock.Object, orderServicePizzaMock.Object);

            //Act

            var result = orderService.CalculateOrder(order, dayOfWeek);

            //Assert

            Assert.AreEqual(90, result);
        }
    }
}

