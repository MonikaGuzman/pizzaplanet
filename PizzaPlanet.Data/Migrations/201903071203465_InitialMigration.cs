namespace PizzaPlanet.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Clients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TelephoneNumber = c.Int(nullable: false),
                        EMail = c.String(),
                        Name = c.String(),
                        Surname = c.String(),
                        Address = c.String(),
                        DataOfLastOrder = c.DateTime(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Igredients",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Pizzas",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        PriceOfSmallSize = c.Double(nullable: false),
                        PriceOfMediumSize = c.Double(nullable: false),
                        PriceOfLargeSize = c.Double(nullable: false),
                        SauceId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Sauces", t => t.SauceId, cascadeDelete: true)
                .Index(t => t.SauceId);
            
            CreateTable(
                "dbo.Orders",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        TotalPrice = c.Double(nullable: false),
                        Client_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Clients", t => t.Client_Id)
                .Index(t => t.Client_Id);
            
            CreateTable(
                "dbo.Sauces",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        Name = c.String(),
                        Price = c.Double(nullable: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.PizzaIgredients",
                c => new
                    {
                        Pizza_Id = c.Int(nullable: false),
                        Igredient_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Pizza_Id, t.Igredient_Id })
                .ForeignKey("dbo.Pizzas", t => t.Pizza_Id, cascadeDelete: true)
                .ForeignKey("dbo.Igredients", t => t.Igredient_Id, cascadeDelete: true)
                .Index(t => t.Pizza_Id)
                .Index(t => t.Igredient_Id);
            
            CreateTable(
                "dbo.OrderPizzas",
                c => new
                    {
                        Order_Id = c.Int(nullable: false),
                        Pizza_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Order_Id, t.Pizza_Id })
                .ForeignKey("dbo.Orders", t => t.Order_Id, cascadeDelete: true)
                .ForeignKey("dbo.Pizzas", t => t.Pizza_Id, cascadeDelete: true)
                .Index(t => t.Order_Id)
                .Index(t => t.Pizza_Id);
            
            CreateTable(
                "dbo.SauceOrders",
                c => new
                    {
                        Sauce_Id = c.Int(nullable: false),
                        Order_Id = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.Sauce_Id, t.Order_Id })
                .ForeignKey("dbo.Sauces", t => t.Sauce_Id, cascadeDelete: true)
                .ForeignKey("dbo.Orders", t => t.Order_Id, cascadeDelete: true)
                .Index(t => t.Sauce_Id)
                .Index(t => t.Order_Id);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Pizzas", "SauceId", "dbo.Sauces");
            DropForeignKey("dbo.SauceOrders", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.SauceOrders", "Sauce_Id", "dbo.Sauces");
            DropForeignKey("dbo.OrderPizzas", "Pizza_Id", "dbo.Pizzas");
            DropForeignKey("dbo.OrderPizzas", "Order_Id", "dbo.Orders");
            DropForeignKey("dbo.Orders", "Client_Id", "dbo.Clients");
            DropForeignKey("dbo.PizzaIgredients", "Igredient_Id", "dbo.Igredients");
            DropForeignKey("dbo.PizzaIgredients", "Pizza_Id", "dbo.Pizzas");
            DropIndex("dbo.SauceOrders", new[] { "Order_Id" });
            DropIndex("dbo.SauceOrders", new[] { "Sauce_Id" });
            DropIndex("dbo.OrderPizzas", new[] { "Pizza_Id" });
            DropIndex("dbo.OrderPizzas", new[] { "Order_Id" });
            DropIndex("dbo.PizzaIgredients", new[] { "Igredient_Id" });
            DropIndex("dbo.PizzaIgredients", new[] { "Pizza_Id" });
            DropIndex("dbo.Orders", new[] { "Client_Id" });
            DropIndex("dbo.Pizzas", new[] { "SauceId" });
            DropTable("dbo.SauceOrders");
            DropTable("dbo.OrderPizzas");
            DropTable("dbo.PizzaIgredients");
            DropTable("dbo.Sauces");
            DropTable("dbo.Orders");
            DropTable("dbo.Pizzas");
            DropTable("dbo.Igredients");
            DropTable("dbo.Clients");
        }
    }
}
