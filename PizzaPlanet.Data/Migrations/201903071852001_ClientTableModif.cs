namespace PizzaPlanet.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class ClientTableModif : DbMigration
    {
        public override void Up()
        {
            RenameTable(name: "dbo.PizzaIgredients", newName: "IgredientPizzas");
            RenameTable(name: "dbo.OrderPizzas", newName: "PizzaOrders");
            DropPrimaryKey("dbo.IgredientPizzas");
            DropPrimaryKey("dbo.PizzaOrders");
            AddPrimaryKey("dbo.IgredientPizzas", new[] { "Igredient_Id", "Pizza_Id" });
            AddPrimaryKey("dbo.PizzaOrders", new[] { "Pizza_Id", "Order_Id" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("dbo.PizzaOrders");
            DropPrimaryKey("dbo.IgredientPizzas");
            AddPrimaryKey("dbo.PizzaOrders", new[] { "Order_Id", "Pizza_Id" });
            AddPrimaryKey("dbo.IgredientPizzas", new[] { "Pizza_Id", "Igredient_Id" });
            RenameTable(name: "dbo.PizzaOrders", newName: "OrderPizzas");
            RenameTable(name: "dbo.IgredientPizzas", newName: "PizzaIgredients");
        }
    }
}
