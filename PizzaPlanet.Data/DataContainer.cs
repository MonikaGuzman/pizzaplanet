﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaPlanet.Data.Interfaces;
using PizzaPlanet.Data.Models;

namespace PizzaPlanet.Data
{
    public class DataContainer : DbContext, IDataContainer
    {
        public DataContainer() : base("PizzeriaDBConnectionString")
        {

        }

        public DbSet<Igredient> Igredients { get; set; }
        public DbSet<Sauce> Sauces { get; set; }
        public DbSet<Pizza> Pizzas { get; set; }
        public DbSet<Order> Orders { get; set; }
        public DbSet<Client> Clients { get; set; }

        public void SaveChangesRepo() { this.SaveChanges(); }
    }
}
