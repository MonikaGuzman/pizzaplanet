﻿using System.Collections.Generic;

namespace PizzaPlanet.Data.Models
{
    public class Pizza
    {
        public Pizza()
        {
            Igredients = new HashSet<Igredient>();
            IngredientsId = new List<int>();
            Orders = new HashSet<Order>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public double PriceOfSmallSize { get; set; }
        public double PriceOfMediumSize { get; set; }
        public double PriceOfLargeSize { get; set; }
        public List<int> IngredientsId { get; set; }
        public int SauceId { get; set; }

        public Sauce Sauce { get; set; }
        public ICollection<Igredient> Igredients { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
