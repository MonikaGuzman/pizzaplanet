﻿using System.Collections.Generic;

namespace PizzaPlanet.Data.Models
{
    public class Igredient
    {
        public Igredient()
        {
            Pizzas = new HashSet<Pizza>();
        }
        public int Id { get; set; }
        public string Name { get; set; }

        public ICollection<Pizza> Pizzas { get; set; }
    }
}
