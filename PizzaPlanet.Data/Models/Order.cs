﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Data.Models
{
    public class Order
    {
        public Order()
        {
            PizzaSizes = new List<string>();
            Pizzas = new HashSet<Pizza>();
            Sauces = new HashSet<Sauce>();
        }
        public int Id { get; set; }
        public List<int> PizzaId { get; set; }
        public List<int> GratisSaucesId { get; set; }
        public List<int> PaidSaucesId { get; set; }
        public List<int> SaucesId { get; set; }
        public List<string> PizzaSizes { get; set; }
        public double TotalPrice { get; set; }
        public Client Client { get; set; }

        public ICollection<Pizza> Pizzas { get; set; }
        public ICollection<Sauce> Sauces { get; set; }
    }
}
