﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Data.Models
{
    public class Client
    {
        public Client()
        {
            //Orders = new HashSet<Order>();
        }
        public int Id { get; set; }
        public int TelephoneNumber { get; set; }
        public string EMail { get; set; }
        public string Name { get; set; }
        public string Surname { get; set; }
        public string Address { get; set; }
        public DateTime DataOfLastOrder { get; set; }

        public ICollection<Order> Orders { get; set; }
    }
}
