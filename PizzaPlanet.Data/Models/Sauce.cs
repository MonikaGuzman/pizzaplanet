﻿using System.Collections.Generic;

namespace PizzaPlanet.Data.Models
{
    public class Sauce
    {
        public Sauce()
        {
            Pizzas = new HashSet<Pizza>();
            Orders = new HashSet<Order>();
        }
        public int Id { get; set; }
        public string Name { get; set; }
        public double Price { get; set; }

        public ICollection<Pizza> Pizzas { get; set; }
        public ICollection<Order> Orders { get; set; }
    }
}
