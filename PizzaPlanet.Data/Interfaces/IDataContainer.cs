﻿using System;
using System.Data.Entity;
using PizzaPlanet.Data.Models;

namespace PizzaPlanet.Data.Interfaces
{
    public interface IDataContainer : IDisposable
    {
        DbSet<Client> Clients { get; set; }
        DbSet<Igredient> Igredients { get; set; }
        DbSet<Order> Orders { get; set; }
        DbSet<Pizza> Pizzas { get; set; }
        DbSet<Sauce> Sauces { get; set; }

        void SaveChangesRepo();
    }
}