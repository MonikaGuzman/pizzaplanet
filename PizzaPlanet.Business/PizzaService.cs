﻿using PizzaPlanet.Business.Helpers;
using PizzaPlanet.Business.Interfaces;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data;
using PizzaPlanet.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;


namespace PizzaPlanet.Business
{
    public class PizzaService : IPizzaService
    {

        private ModelsMapper _modelsMapper;

        private readonly Func<IDataContainer> _dataContainer;

        public PizzaService(Func<IDataContainer> dataContainer)
        {
            _dataContainer = dataContainer;
            _modelsMapper = new ModelsMapper(_dataContainer);
        }

        public void AddPizza(PizzaBl pizza)
        {
            using (var dataContainer = _dataContainer())
            {
                bool isCorrect = true;
                int countIngredients = pizza.IngredientsId.Count;
                bool doesPizzaExist = dataContainer.Pizzas.Any(p => p.Name == pizza.Name);
                bool doesSauseExist = dataContainer.Sauces.Any(s => s.Id == pizza.SauceId);

                if (dataContainer.Igredients.Count() < 1 || dataContainer.Sauces.Count() < 1)
                {
                    isCorrect = false;
                    throw new ArgumentException("Nie masz wystarczajacej ilosci skladnikow i/lub sosow do stworzenia pizzy");
                }

                if (doesPizzaExist == true)
                {
                    isCorrect = false;
                    throw new ArgumentException("Taka pizza juz istnieje");
                }

                if (string.IsNullOrWhiteSpace(pizza.Name))
                {
                    isCorrect = false;
                    throw new ArgumentException("Nie mozna dodac pizzy bez nazwy");
                }

                if (pizza.SauceId == 0)
                {
                    isCorrect = false;
                    throw new ArgumentException("Nie mozna dodac pizzy bez sosu");
                }

                if (doesSauseExist == false)
                {
                    isCorrect = false;
                    throw new ArgumentException("Wybrany sos nie istnieje");
                }

                if (countIngredients < 1 || countIngredients > 7)
                {
                    isCorrect = false;
                    throw new ArgumentException("Podana liczba skladnikow nie miesci sie w dozwolonym zakresie");
                }

                foreach (var ingredient in pizza.IngredientsId)
                {
                    bool doesIngredientExist = dataContainer.Igredients.Any(i => i.Id == ingredient);
                    if (doesIngredientExist == false)
                    {
                        isCorrect = false;
                        throw new ArgumentException("Wybrany skladnik nie istnieje");
                    }
                }

                var count = 1;
                for (int i = 0; i < pizza.IngredientsId.Count; i++)
                {
                    for (int k = i + 1; k < pizza.IngredientsId.Count; k++)
                    {
                        if (pizza.IngredientsId[i] == pizza.IngredientsId[k])
                        {
                            {
                                count++;
                            }
                        }

                        if (count > 2)
                        {
                            isCorrect = false;
                            throw new ArgumentException("Jeden ze skladnikow zostal wybrany wiecej niz dwa razy");
                        }
                    }
                }

                if (pizza.PriceOfSmallSize == pizza.PriceOfMediumSize)
                {
                    isCorrect = false;
                    throw new ArgumentException("Cena pizzy w rozmiarze S musi byc inna niz cena pizzy w rozmiarze M");
                }

                if (pizza.PriceOfSmallSize == pizza.PriceOfLargeSize)
                {
                    isCorrect = false;
                    throw new ArgumentException("Cena pizzy w rozmiarze S musi byc inna niz cena pizzy w rozmiarze L");
                }

                if (pizza.PriceOfMediumSize == pizza.PriceOfLargeSize)
                {
                    isCorrect = false;
                    throw new ArgumentException("Cena pizzy w rozmiarze M musi byc inna niz cena pizzy w rozmiarze L");
                }

                if (isCorrect == true)
                {
                    var pizzaToAdd = _modelsMapper.MapPizzaFromBusinessToDataModel(pizza);

                    foreach (var ingredient in pizzaToAdd.Igredients)
                    {
                        dataContainer.Igredients.Attach(ingredient);
                    }

                    dataContainer.Pizzas.Add(pizzaToAdd);
                    dataContainer.SaveChangesRepo();
                }
            }

        }

        public IEnumerable<PizzaBl> GetAllPizzas()
        {
            using (var dataContainer = _dataContainer())
            {
                var pizzaBl = dataContainer.Pizzas.Include(p => p.Igredients).ToList().Select(p => _modelsMapper.MapPizzaFromDataToBusinessModel(p));
                return pizzaBl;
            }          
        }
    }
}
