﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaPlanet.Business.Helpers;
using PizzaPlanet.Business.Interfaces;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data;
using PizzaPlanet.Data.Interfaces;

namespace PizzaPlanet.Business
{
    public class ClientService : IClientService
    {
        private ModelsMapper _modelsMapper;

        private readonly Func<IDataContainer> _dataContainer;

        public ClientService(Func<IDataContainer> dataContainer)
        {
            _dataContainer = dataContainer;
            _modelsMapper = new ModelsMapper(_dataContainer);
        }

        public ClientBl AddClient(ClientBl client)
        {
            using (var dataContainer = _dataContainer())
            {
                bool isCorrect = true;
                bool doesClientExist = dataContainer.Clients.Any(c => c.TelephoneNumber == client.TelephoneNumber);
                if (doesClientExist == true)
                {
                    isCorrect = false;
                    throw new ArgumentException("Taki klient juz istnieje!");
                }

                bool doesClientEmailExist = dataContainer.Clients.Any(c => c.EMail == client.EMail);

                if (doesClientEmailExist == true)
                {
                    isCorrect = false;
                    throw new ArgumentException("Taki e-mail juz istnieje!");
                }

                if (string.IsNullOrWhiteSpace(client.Name))
                {
                    isCorrect = false;
                    throw new ArgumentException("Nie mozna dodac klienta bez imienia");
                }

                if (string.IsNullOrWhiteSpace(client.Surname))
                {
                    isCorrect = false;
                    throw new ArgumentException("Nie mozna dodac klienta bez nazwiska");
                }

                if (string.IsNullOrWhiteSpace(client.Address))
                {
                    isCorrect = false;
                    throw new ArgumentException("Nie mozna dodac klienta bez adresu");
                }

                if (string.IsNullOrWhiteSpace(client.EMail))
                {
                    isCorrect = false;
                    throw new ArgumentException("Nie mozna dodac klienta bez podawania adresu email");
                }

                if (isCorrect == true)
                {
                    var clientToAdd = _modelsMapper.MapClientFromBusinessToDataModel(client);
                    var addedClient = dataContainer.Clients.Add(clientToAdd);
                    dataContainer.SaveChangesRepo();

                    return _modelsMapper.MapClientFromDataToBusinessModel(addedClient);
                }

                return null;
            }
        }

        public bool CheckingIfClientExist(ClientBl client)
        {
            using (var dataContainer = _dataContainer())
            {
                var clientToCheck = _modelsMapper.MapClientFromBusinessToDataModel(client);
                bool doesClientNumberExist = dataContainer.Clients.Any(c => c.TelephoneNumber == clientToCheck.TelephoneNumber);

                return doesClientNumberExist;
            }
        }

        public void UpdateDataInClient(int clientNr, DateTime lastOrderDate)
        {
            using (var dataContainer = _dataContainer())
            {
                var client = dataContainer.Clients.SingleOrDefault(x => x.TelephoneNumber == clientNr);
                client.DataOfLastOrder = lastOrderDate;
                dataContainer.SaveChangesRepo();
            }
        }
    }
}
