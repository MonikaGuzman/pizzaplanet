﻿namespace PizzaPlanet.Business.Models
{
    public class IgredientBl
    {
        public int Id { get; set; }
        public string Name { get; set; }
    }
}
