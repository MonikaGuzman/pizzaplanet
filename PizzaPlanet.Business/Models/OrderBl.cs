﻿using PizzaPlanet.Data.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business.Models
{
    public class OrderBl
    {
        public int Id { get; set; }
        public List<int> PizzaId { get; set; }
        public List<int> GratisSaucesId { get; set; }
        public List<int> PaidSaucesId { get; set; }
        public List<int> SaucesId { get; set; }
        public List<string> PizzaSizes { get; set; }
        public double TotalPrice { get; set; }
        public ClientBl Client { get; set; }

    }
}
