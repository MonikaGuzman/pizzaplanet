﻿using System.Collections.Generic;

namespace PizzaPlanet.Business.Models
{
    public class PizzaBl
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public double PriceOfSmallSize { get; set; }
        public double PriceOfMediumSize { get; set; }
        public double PriceOfLargeSize { get; set; }
        public List<int> IngredientsId { get; set; }
        public int SauceId { get; set; }
    }
}
