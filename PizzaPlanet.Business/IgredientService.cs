﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaPlanet.Business.Helpers;
using PizzaPlanet.Business.Interfaces;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data;
using PizzaPlanet.Data.Interfaces;

namespace PizzaPlanet.Business
{
    public class IgredientService : IIgredientService
    {
        private ModelsMapper _modelsMapper;

        private readonly Func<IDataContainer> _dataContainer;

        public IgredientService(Func<IDataContainer> dataContainer)
        {
            _dataContainer = dataContainer;
            _modelsMapper = new ModelsMapper(_dataContainer);
        }

        public void AddIgredient(IgredientBl igredient)
        {
            using (var dataContainer = _dataContainer())
            {
                bool isCorrect = true;
                bool doesIngredientExist = dataContainer.Igredients.Any(i => i.Name == igredient.Name);
                if (doesIngredientExist == true)
                {
                    isCorrect = false;
                    throw new ArgumentException("Taki skladnik juz istnieje!");
                }
                if (string.IsNullOrWhiteSpace(igredient.Name))
                {
                    isCorrect = false;
                    throw new ArgumentException("Nie mozna dodac skladnika bez nazwy");
                }
                if (isCorrect == true)
                {
                    var igredientToAdd = _modelsMapper.MapIgredientFromBusinessToDataModel(igredient);
                    dataContainer.Igredients.Add(igredientToAdd);
                    dataContainer.SaveChangesRepo();
                }
            }
        }

        public IEnumerable<IgredientBl> GetAllIgredients()
        {
            using (var dataContainer = _dataContainer())
            {
                var igredientsBl = dataContainer.Igredients.ToList().Select(i => _modelsMapper.MapIgredientFromDataToBusinessModel(i));
                return igredientsBl;
            }
        }
    }
}
