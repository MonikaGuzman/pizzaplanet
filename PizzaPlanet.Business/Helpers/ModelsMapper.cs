﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data.Interfaces;
using PizzaPlanet.Data.Models;

namespace PizzaPlanet.Business.Helpers
{
    internal class ModelsMapper
    {
        private readonly Func<IDataContainer> db;

        public ModelsMapper(Func<IDataContainer> dataContainer)
        {
            db = dataContainer;
        }

        public IgredientBl MapIgredientFromDataToBusinessModel(Igredient igredient)
        {
            return new IgredientBl()
            {
                Id = igredient.Id,
                Name = igredient.Name
            };
        }

        public Igredient MapIgredientFromBusinessToDataModel(IgredientBl igredientBl)
        {
            return new Igredient()
            {
                Id = igredientBl.Id,
                Name = igredientBl.Name
            };
        }

        public SauceBl MapSauceFromDataToBusinessModel(Sauce sauce)
        {
            return new SauceBl()
            {
                Id = sauce.Id,
                Name = sauce.Name,
                Price = sauce.Price
            };
        }

        public Sauce MapSauceFromBusinessToDataModel(SauceBl sauceBl)
        {
            return new Sauce()
            {
                Id = sauceBl.Id,
                Name = sauceBl.Name,
                Price = sauceBl.Price
            };
        }

        public PizzaBl MapPizzaFromDataToBusinessModel(Pizza pizza)
        {
            return new PizzaBl()
            {
                Id = pizza.Id,
                Name = pizza.Name,
                SauceId = pizza.SauceId,
                IngredientsId = pizza.Igredients.Select(i => i.Id).ToList(),
                PriceOfSmallSize = pizza.PriceOfSmallSize,
                PriceOfMediumSize = pizza.PriceOfMediumSize,
                PriceOfLargeSize = pizza.PriceOfLargeSize
            };
        }

        public Pizza MapPizzaFromBusinessToDataModel(PizzaBl pizzaBl)
        {
            return new Pizza()
            {
                Id = pizzaBl.Id,
                Name = pizzaBl.Name,
                SauceId = pizzaBl.SauceId,
                IngredientsId = pizzaBl.IngredientsId,
                Igredients = db().Igredients.ToList().Where(x => pizzaBl.IngredientsId.Any(g => g == x.Id)).ToList(),
                PriceOfSmallSize = pizzaBl.PriceOfSmallSize,
                PriceOfMediumSize = pizzaBl.PriceOfMediumSize,
                PriceOfLargeSize = pizzaBl.PriceOfLargeSize
            };
        }

        public OrderBl MapOrderFromDataToBusinessModel(Order order)
        {
            return new OrderBl()
            {
                Id = order.Id,
                PizzaId = order.Pizzas.Select(p => p.Id).ToList(),
                GratisSaucesId = order.GratisSaucesId,
                PaidSaucesId = order.PaidSaucesId,
                SaucesId = order.Sauces.Select(s => s.Id).ToList(),
                PizzaSizes = order.PizzaSizes.ToList(),
                TotalPrice = order.TotalPrice,
                Client = MapClientFromDataToBusinessModel(order.Client)
            };
        }

        public Order MapOrderFromBusinessToDataModel(OrderBl orderBl)
        {
            return new Order()
            {
                Id = orderBl.Id,
                PizzaId = orderBl.PizzaId,
                Pizzas = db().Pizzas.ToList().Where(x => orderBl.PizzaId.Any(z => z == x.Id)).ToList(),
                GratisSaucesId = orderBl.GratisSaucesId,
                PaidSaucesId = orderBl.PaidSaucesId,
                SaucesId = orderBl.SaucesId,
                Sauces = db().Sauces.ToList().Where(x => orderBl.SaucesId.Any(z => z == x.Id)).ToList(),
                PizzaSizes = orderBl.PizzaSizes,
                TotalPrice = orderBl.TotalPrice,
                Client = MapClientFromBusinessToDataModel(orderBl.Client)
            };
        }

        public ClientBl MapClientFromDataToBusinessModel(Client client)
        {
            return new ClientBl()
            {
                Id = client.Id,
                TelephoneNumber = client.TelephoneNumber,
                EMail = client.EMail,
                Name = client.Name,
                Surname = client.Surname,
                Address = client.Address,
                DataOfLastOrder = client.DataOfLastOrder
            };
        }

        public Client MapClientFromBusinessToDataModel(ClientBl clientBl)
        {
            return new Client()
            {
                Id = clientBl.Id,
                TelephoneNumber = clientBl.TelephoneNumber,
                EMail = clientBl.EMail,
                Name = clientBl.Name,
                Surname = clientBl.Surname,
                Address = clientBl.Address,
                DataOfLastOrder = clientBl.DataOfLastOrder
            };
        }
    }
}
