﻿using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using PizzaPlanet.Business.Helpers;
using PizzaPlanet.Business.Interfaces;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data;
using PizzaPlanet.Data.Interfaces;

namespace PizzaPlanet.Business
{
    public class OrderService : IOrderService
    {
        private ModelsMapper _modelsMapper;

        public string dayOfWeek = DateTime.UtcNow.DayOfWeek.ToString();

        private readonly Func<IDataContainer> _dataContainer;

        private ISauceService _sauceService;
        private IPizzaService _pizzaService;
        public OrderService(Func<IDataContainer> dataContainer, ISauceService sauceService, IPizzaService pizzaService)
        {
            _dataContainer = dataContainer;
            _modelsMapper = new ModelsMapper(_dataContainer);
            _sauceService = sauceService;
            _pizzaService = pizzaService;
        }

        public void AddPizzaToOrder(OrderBl order)
        {
            using (var dataContainer = _dataContainer())
            {
                int countPizzas = dataContainer.Pizzas.Count();

                if (countPizzas == 0)
                {
                    throw new ArgumentException("System nie ma zadnej pizzy");
                }

                foreach (var pizza in order.PizzaId)
                {
                    bool doesPizzaExist = dataContainer.Pizzas.Any(p => p.Id == pizza);
                    if (doesPizzaExist == false)
                    {
                        throw new ArgumentException("Wybrana pizza nie istnieje");
                    }
                }
            }
        }

        public void AddGratisSaucesToOrder(OrderBl order)
        {
            using (var dataContainer = _dataContainer())
            {
                int countChosenPizza = order.PizzaId.Count();
                int countGratisSauces = GratisSaucesCount(order);

                if (order.GratisSaucesId.Count > countGratisSauces)
                {
                    throw new ArgumentException("Wybrales za duzo darmowych sosow");
                }

                foreach (var sauce in order.GratisSaucesId)
                {
                    bool doesSaucesExist = dataContainer.Sauces.Any(s => s.Id == sauce);
                    if (doesSaucesExist == false)
                    {
                        throw new ArgumentException("Wybrane sosy nie istnieja");
                    }
                }
            }
        }

        public void AddOrder(OrderBl order)
        {
            using (var dataContainer = _dataContainer())
            {
                var orderToAdd = _modelsMapper.MapOrderFromBusinessToDataModel(order);

                foreach (var sauce in orderToAdd.Sauces)
                {
                    dataContainer.Sauces.Attach(sauce);
                }

                foreach (var pizza in orderToAdd.Pizzas)
                {
                    dataContainer.Pizzas.Attach(pizza);
                }

                orderToAdd.Client = dataContainer.Clients.SingleOrDefault(x => x.TelephoneNumber == orderToAdd.Client.TelephoneNumber);
                dataContainer.Clients.Attach(orderToAdd.Client);

                dataContainer.Orders.Add(orderToAdd);
                dataContainer.SaveChangesRepo();
            }
        }

        public double CalculateOrder(OrderBl order, string dayOfWeek)
        {
            double totalPrice;

            if (order.PaidSaucesId == null)
            {
                return totalPrice = TotalPriceOfOrderWithoutPaidSauces(order, dayOfWeek);
            }
            else
            {
                return totalPrice = TotalPriceOfOrderWithoutPaidSauces(order, dayOfWeek) + TotalPriceOfOrederedPaidSauces(order);
            }
        }

        public int GratisSaucesCount(OrderBl order)
        {
            var count = 0;
            for (int i = 0; i < order.PizzaSizes.Count; i++)
            {
                if (order.PizzaSizes[i] == "S")
                {
                    count++;
                }
                if (order.PizzaSizes[i] == "M")
                {
                    count++;
                }
                if (order.PizzaSizes[i] == "L")
                {
                    count = count + 2;
                }
            }
            return count;
        }

        public double TotalPriceOfOrderWithoutPaidSauces(OrderBl order, string dayOfWeek)
        {
            List<double> pricesOfChosenSmallSizePizzas = new List<double>();
            List<double> pricesOfChosenMediumSizePizzas = new List<double>();
            List<double> pricesOfChosenLargeSizePizzas = new List<double>();
            double countTotalPizzasPrice;

            for (int i = 0; i < order.PizzaId.Count; i++)
            {
                if (order.PizzaSizes[i] == "S")
                {
                    foreach (var pizza in _pizzaService.GetAllPizzas())
                    {
                        if (order.PizzaId[i] == pizza.Id)
                        {
                            pricesOfChosenSmallSizePizzas.Add(pizza.PriceOfSmallSize);
                        }
                    }
                }

                if (order.PizzaSizes[i] == "M")
                {
                    foreach (var pizza in _pizzaService.GetAllPizzas())
                    {
                        if (order.PizzaId[i] == pizza.Id)
                        {
                            pricesOfChosenMediumSizePizzas.Add(pizza.PriceOfMediumSize);
                        }
                    }
                }

                if (order.PizzaSizes[i] == "L")
                {
                    foreach (var pizza in _pizzaService.GetAllPizzas())
                    {
                        if (order.PizzaId[i] == pizza.Id)
                        {
                            if (dayOfWeek == "Wednesday")
                            {
                                pricesOfChosenLargeSizePizzas.Add(pizza.PriceOfMediumSize);
                            }

                            else
                            {
                                pricesOfChosenLargeSizePizzas.Add(pizza.PriceOfLargeSize);
                            }
                        }
                    }
                }
            }

            if (dayOfWeek == "Sunday" || dayOfWeek == "Saturday")
            {
                return countTotalPizzasPrice = (pricesOfChosenSmallSizePizzas.Sum() + pricesOfChosenMediumSizePizzas.Sum() + pricesOfChosenLargeSizePizzas.Sum()) / 2;
            }

            else
            {
                return countTotalPizzasPrice = pricesOfChosenSmallSizePizzas.Sum() + pricesOfChosenMediumSizePizzas.Sum() + pricesOfChosenLargeSizePizzas.Sum();
            }
        }

        public double TotalPriceOfOrederedPaidSauces(OrderBl order)
        {
            double countTotalSaucesPrice;
            List<double> pricesOfChosenSauces = new List<double>();

            for (int i = 0; i < order.PaidSaucesId.Count; i++)
            {
                foreach (var sauce in _sauceService.GetAllSauces())
                {
                    if (order.PaidSaucesId[i] == sauce.Id)
                    {
                        pricesOfChosenSauces.Add(sauce.Price);
                    }
                }
            }
            return countTotalSaucesPrice = pricesOfChosenSauces.Sum();

        }
    }
}
