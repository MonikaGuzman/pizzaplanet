﻿using PizzaPlanet.Business.Helpers;
using PizzaPlanet.Business.Interfaces;
using PizzaPlanet.Business.Models;
using PizzaPlanet.Data;
using PizzaPlanet.Data.Interfaces;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business
{
    public class SauceService : ISauceService
    {
        private ModelsMapper _modelsMapper;

        private readonly Func<IDataContainer> _dataContainer;

        public SauceService(Func<IDataContainer> dataContainer)
        {
            _dataContainer = dataContainer;
            _modelsMapper = new ModelsMapper(_dataContainer);
        }

        public void AddSauce(SauceBl sauce)
        {
            using (var dataContainer = _dataContainer())
            {
                bool isCorrect = true;
                bool doesSaucesExist = dataContainer.Sauces.Any(s => s.Name == sauce.Name);
                if (doesSaucesExist == true)
                {
                    isCorrect = false;
                    throw new ArgumentException("Taki sos juz istnieje!");
                }
                if (string.IsNullOrWhiteSpace(sauce.Name))
                {
                    isCorrect = false;
                    throw new ArgumentException("Nie mozna dodac sosu bez nazwy");
                }
                if (isCorrect == true)
                {
                    var sauceToAdd = _modelsMapper.MapSauceFromBusinessToDataModel(sauce);
                    dataContainer.Sauces.Add(sauceToAdd);
                    dataContainer.SaveChangesRepo();
                }
            }
        }

        public IEnumerable<SauceBl> GetAllSauces()
        {
            using (var dataContainer = _dataContainer())
            {
                var saucesBl = dataContainer.Sauces.ToList().Select(s => _modelsMapper.MapSauceFromDataToBusinessModel(s));
                return saucesBl;
            }
        }

    }
}
