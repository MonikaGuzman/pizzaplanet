﻿using PizzaPlanet.Business.Models;
using System;

namespace PizzaPlanet.Business.Interfaces
{
    public interface IOrderService
    {
        void AddGratisSaucesToOrder(OrderBl order);
        void AddOrder(OrderBl order);
        void AddPizzaToOrder(OrderBl order);
        double CalculateOrder(OrderBl order, string dayOfWeek);
        int GratisSaucesCount(OrderBl order);
        double TotalPriceOfOrderWithoutPaidSauces(OrderBl order, string dayOfWeek);
        double TotalPriceOfOrederedPaidSauces(OrderBl order);
    }
}