﻿using System.Collections.Generic;
using PizzaPlanet.Business.Models;

namespace PizzaPlanet.Business.Interfaces
{
    public interface ISauceService
    {
        void AddSauce(SauceBl sauce);
        IEnumerable<SauceBl> GetAllSauces();
    }
}