﻿using System.Collections.Generic;
using PizzaPlanet.Business.Models;

namespace PizzaPlanet.Business.Interfaces
{
    public interface IPizzaService
    {
        void AddPizza(PizzaBl pizza);
        IEnumerable<PizzaBl> GetAllPizzas();
    }
}