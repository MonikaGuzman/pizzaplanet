﻿using PizzaPlanet.Business.Models;
using System;

namespace PizzaPlanet.Business.Interfaces
{
    public interface IClientService
    {
        ClientBl AddClient(ClientBl client);
        bool CheckingIfClientExist(ClientBl client);
        void UpdateDataInClient(int clientId, DateTime lastOrderDate);
    }
}