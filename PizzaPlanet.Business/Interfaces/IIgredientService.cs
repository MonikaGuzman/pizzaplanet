﻿using System.Collections.Generic;
using PizzaPlanet.Business.Models;

namespace PizzaPlanet.Business.Interfaces
{
    public interface IIgredientService
    {
        void AddIgredient(IgredientBl igredient);
        IEnumerable<IgredientBl> GetAllIgredients();
    }
}