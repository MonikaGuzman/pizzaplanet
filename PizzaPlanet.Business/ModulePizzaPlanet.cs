﻿using Ninject.Modules;
using PizzaPlanet.Business.Interfaces;
using PizzaPlanet.Data;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection;
using System.Text;
using System.Threading.Tasks;

namespace PizzaPlanet.Business
{
    public class ModulePizzaPlanet : NinjectModule
    {
        public override void Load()
        {
            Kernel.Bind<IIgredientService>().To<IgredientService>();
            Kernel.Bind<IOrderService>().To<OrderService>();
            Kernel.Bind<IPizzaService>().To<PizzaService>();
            Kernel.Bind<ISauceService>().To<SauceService>();
            Kernel.Bind<IClientService>().To<ClientService>();
            Kernel.Load(new List<INinjectModule>() {new ModuleData() });
        }
    }
}
